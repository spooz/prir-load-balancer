<!DOCTYPE HTML>
<html>
<head>
    <title>Spring Freemarker</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>

</head>
<body>
    <div style="max-width: 1000px; margin:auto;">
        <h3>Servers</h3>

        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add server
                </div>
                <div class="panel-body">
                <form name="server" action="/server/add" method="POST">

                    <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input type="text" class="form-control" name="name" required >
                    </div>

                    <div class="form-group">
                        <label for="ip">
                            IP
                        </label>
                        <input type="text" class="form-control" name="ip" required>
                    </div>

                    <div class="form-group">
                        <label for="port">
                           Port
                        </label>
                        <input type="number" class="form-control" name="port" required>
                    </div>

                    <div class="form-group">
                        <label for="taskPath">
                            Task path
                        </label>
                        <input type="text" class="form-control" name="taskPath" required >
                    </div>

                    <div class="form-group">
                        <label for="maxLoad">
                            Max load
                        </label>
                        <input type="number" class="form-control" name="maxLoad" required >
                    </div>
                    <button type="submit" class="btn btn-default">Save</button>
                </form>
                </div>
            </div>
        </div>

        <div class="row">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Server name</th>
                    <th>Server path</th>
                    <th>Server running tasks</th>
                    <th>Server maxLoad</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>

                    <#list servers as server>
                        <tr>
                            <td>${server.name}</td>
                            <td>${server.ip}:${server.port}${server.taskPath}</td>
                            <td>${server.runningTasks}</td>
                            <td>${server.maxLoad}</td>
                            <td>
                                <a href="/server/${server.id}/delete" role="button" class="btn btn-danger">Delete</a>
                                </td>
                        </tr>
                    </#list>
                </tbody>
            </table>
        </div>


    <div class="row">
        <h3>Tasks</h3>
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Task id</th>
                <th>Task server</th>
                <th>Task status</th>
                <th>Start time</th>
                <th>End time</th>
            </tr>
            </thead>
            <tbody>

            <#list tasks as task>
            <tr class="
                <#if task.status == "DONE">
                  success
                  <#elseif task.status == "PROCESSING">
                    info
                  <#else>
                    danger
                </#if>



            ">
                <td>${task.id}</td>
                <td>${task.serverIp}</td>
                <td>${task.status}</td>
                <td>${task.startTime}</td>
                <td>${task.endTime}</td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
    </div>
    </div>

    <script>
        $("#datatable").DataTable( {
            "order": [[ 3, "desc" ]]
        });

    </script>
</body>
</html>