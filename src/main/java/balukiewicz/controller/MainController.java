package balukiewicz.controller;

import balukiewicz.Model.Server;
import balukiewicz.Model.Task;
import balukiewicz.Repository.ServerRepository;
import balukiewicz.Repository.TaskRepository;
import balukiewicz.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
* Created by Bartek on 05.06.2016.
        */
@Controller
public class MainController {

    @Autowired
    private ServerService serverService;

    @Autowired
    private TaskRepository taskRepository;

    RestTemplate rest = new RestTemplate();

    @RequestMapping(value ="/server/add", method = RequestMethod.POST)
    public String addServer(@ModelAttribute Server server) {
        serverService.save(server);
        return "redirect:/";
    }

    @RequestMapping(value="/")
    public String getServers(Model model) {
        model.addAttribute("servers", serverService.getAllServersWithRunningTasks());
        model.addAttribute("tasks", taskRepository.findAll());
        return "servers";
    }

    @RequestMapping(value="/server/{serverId}/delete")
    public String deleteServer(@PathVariable String serverId) {
        serverService.delete(serverId);
        return "redirect:/";
    }





}
