package balukiewicz.controller;

import balukiewicz.Model.Server;
import balukiewicz.Model.Task;
import balukiewicz.Repository.ServerRepository;
import balukiewicz.Repository.TaskRepository;
import balukiewicz.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bartek on 06.06.2016.
 */
@Controller
public class LoadBalancerEndpoint {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value ="/endpoint")
    public @ResponseBody String response() throws InterruptedException {
        return taskService.handleRequest();
    }

}

