package balukiewicz.service;

import balukiewicz.Model.Server;
import balukiewicz.Model.Task;
import balukiewicz.Repository.ServerRepository;
import balukiewicz.Repository.TaskRepository;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Bartek on 10.06.2016.
 */
@Service
public class TaskService {

    private static final Logger LOGGER = Logger.getLogger(TaskService.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ServerService serverService;

    private RestTemplate restTemplate = new RestTemplate();

    public String handleRequest() throws InterruptedException {

       LOGGER.debug("BALANCING TASK");

        boolean check = true;
        Server bestServer = null;
        Integer minLoad = 999999;
        while(check) {
            Iterator<String> iterator = serverService.getRunningServerTasksKeySet().iterator();
            List<Server> servers = serverService.findAll();
            for(Server server : servers) {
                Integer currentLoad = serverService.getRunningTasksForServer(server);
                if(server.getMaxLoad() - currentLoad <= 0)
                    continue;
                else {
                    if(currentLoad < minLoad) {
                        bestServer = server;
                        serverService.updateServerRunningTasksAdd(server);
                        break;
                    }
                }
            }

            if(bestServer != null)
                check = false;
            else
                Thread.sleep(5000);
        }

        LOGGER.debug("CHOOSEN SERVER : " + bestServer.getId());


        Task task = new Task();
        task.setStatus(Task.TASK_STATUS.PROCESSING);
        task.setServerIp(bestServer.getIp() + ":" + bestServer.getPort());
        task.setStartTime(LocalDateTime.now().toString());
        taskRepository.save(task);

       // bestServer.setRunningTasks(bestServer.getRunningTasks() + 1);
        //serverRepository.save(bestServer);


        ResponseEntity<String> response = null;

        LOGGER.debug(bestServer.getId() + " SENDING REQUEST");
        try {
             response = restTemplate.getForEntity("http://" + bestServer.getIp() + ":" + bestServer.getPort() + bestServer.getTaskPath(), String.class);
        } catch (RestClientException e) {
            task.setStatus(Task.TASK_STATUS.FAILED);
            task.setEndTime(LocalDateTime.now().toString());
            taskRepository.save(task);
            return e.getMessage();

        }
        task.setStatus(Task.TASK_STATUS.DONE);
        task.setEndTime(LocalDateTime.now().toString());
        taskRepository.save(task);

        LOGGER.debug(bestServer.getId() + " GOT REQUEST");


       serverService.updateServerRunningTasksRemove(bestServer);

        return response.getBody();
    }
}
