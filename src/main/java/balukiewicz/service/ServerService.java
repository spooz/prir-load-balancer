package balukiewicz.service;

import balukiewicz.Model.Server;
import balukiewicz.Repository.ServerRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Bartek on 11.06.2016.
 */

@Service
public class ServerService {


    private static final Logger LOGGER = Logger.getLogger(ServerService.class);

    private Map<String, Integer> runningServerTasks = new HashMap<>();

    @PostConstruct
    private void getServers() {
        List<Server> servers = serverRepository.findAll();
        for(Server server : servers) {
            runningServerTasks.put(server.getId(), 0);
            LOGGER.debug("ADDED SERVER TO CACHE " + server.toString());
        }
    }

   @Autowired
    private ServerRepository serverRepository;

    public Server findById(String id) {
        return serverRepository.findOne(id);
    }

    public List<Server> findAll() {
        return serverRepository.findAll();
    }

    public void save(Server server) {
        serverRepository.save(server);
        runningServerTasks.put(server.getId(), 0);
    }

    public void delete(String server) {
        runningServerTasks.remove(server);
        serverRepository.delete(server);
    }

    public List<Server> getAllServersWithRunningTasks() {

        List<Server> servers = serverRepository.findAll();
        for(Server server : servers) {
            Integer runningTasks = getRunningTasksForServer(server);
            if(runningTasks == null)
                runningTasks = 0;
            server.setRunningTasks(runningTasks);
            LOGGER.debug("RUNNING TASKS FOR SERVER: " + runningTasks);
        }

        return servers;

    }

    public Set<String> getRunningServerTasksKeySet() {

        return runningServerTasks.keySet();
    }

    public Integer getRunningTasksForServer(Server server) {
        LOGGER.debug(runningServerTasks.toString());
        LOGGER.debug(runningServerTasks.get(server.getId()));
        return runningServerTasks.get(server.getId());
    }

    @Transactional
    public void updateServerRunningTasksAdd(Server server) {
        Integer currentRunningTasks = runningServerTasks.get(server.getId()) + 1;
        LOGGER.debug("ADDED RUNNING TASK TO SERVER. CURRENT: " + currentRunningTasks);
        runningServerTasks.put(server.getId(), currentRunningTasks );
    }

    @Transactional
    public void updateServerRunningTasksRemove(Server server) {
        Integer currentRunningTasks = runningServerTasks.get(server.getId());
        runningServerTasks.put(server.getId(), currentRunningTasks - 1);
    }
}
