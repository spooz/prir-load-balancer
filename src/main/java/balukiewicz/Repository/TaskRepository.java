package balukiewicz.Repository;

import balukiewicz.Model.Server;
import balukiewicz.Model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Bartek on 10.06.2016.
 */
public interface TaskRepository extends MongoRepository<Task, String> {
}
