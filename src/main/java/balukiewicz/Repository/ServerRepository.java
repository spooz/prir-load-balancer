package balukiewicz.Repository;

import balukiewicz.Model.Server;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bartek on 06.06.2016.
 */
@Repository
public interface ServerRepository extends MongoRepository<Server, String> {

}
